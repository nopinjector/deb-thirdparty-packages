# deb-thirdparty-packages

**deb-thirdparty-packages** is a collection of Debian build and packaging scripts for packaging 3rd-party software
only available in binary and non-Debian compatible package formats (like *.tar.gz) into a Debian package.

This should ease the tedious process of updating such software manually by providing the applications in standard
Debian packages which can be distributed via Debian repositories (**please** take care of the applicable license terms). 

**Ensure, that you read the applicable license terms (EULA) on the vendors website before installing!**

Currently, this repository contains packaging scripts for:

* [Android Studio](https://developer.android.com/studio)
* [Burp Suite Community Edition](https://portswigger.net/burp)
* JetBrains IDEs:
  * [JetBrains CLion](https://www.jetbrains.com/clion/)
  * [Jetbrains DataGrip](https://www.jetbrains.com/datagrip/)
  * [Jetbrains DataSpell](https://www.jetbrains.com/dataspell/)
  * [JetBrains GoLand](https://www.jetbrains.com/go/)
  * [JetBrains IntelliJ IDEA](https://www.jetbrains.com/idea/) (Ultimate & Community)
  * [JetBrains PhpStorm](https://www.jetbrains.com/phpstorm/)
  * [JetBrains PyCharm](https://www.jetbrains.com/pycharm/) (Professional & Community)
  * [JetBrains Rider](https://www.jetbrains.com/rider/)
  * [JetBrains RubyMine](https://www.jetbrains.com/ruby/)
  * [JetBrains WebStorm](https://www.jetbrains.com/webstorm/)
* [SoftMaker Office](https://www.softmaker.com/en/softmaker-office) *[SoftMaker also publishes official Debian packages on its [website](https://www.softmaker.de/en/softmaker-office-download)]*
* [The Grml project's zsh config](https://grml.org/zsh/)

I'm not affiliated with any of the companies or projects nor they are with me.

# Usage

Run `build.sh` with the repository root as working directory to build packages for all software listed above. Invoking
`build.sh` in any subfolder will limit the packaging process to the contained software.

The build artifacts can always be found in `<repository-root>/_artifacts`.

# Prerequisites

All packages created by the scripts in this repository have their specific dependencies which are not listed here.
Besides the scripts from this repository have the following prerequisites:

* jq
* rsync
* wget
* dpkg-deb
* debhelper
* config-package-dev