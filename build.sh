#!/bin/bash -e

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
PWD=$(realpath "$(pwd)")

ARTIFACTS_DIR="${SCRIPT_DIR}/_artifacts"

function check_prerequisites {
  command -v jq >/dev/null 2>&1 || (echo "jq required, but not found. exiting!"; exit 127)
  command -v rsync >/dev/null 2>&1 || (echo "rsync required, but not found. exiting!"; exit 127)
  command -v dpkg-buildpackage >/dev/null 2>&1 || (echo "dpkg-buildpackage required, but not found. exiting!"; exit 127)
  command -v dh >/dev/null 2>&1 || (echo "debhelper required, but not found. exiting!"; exit 127)
}

function unshare_me {
  if [ $(id -u) -eq 0 ]; then
    return
  fi

  if [ $(cat /proc/sys/kernel/unprivileged_userns_clone) -eq 0 ]; then
    echo "please enable unprivileged_userns_clone or run as root! exiting..."
    exit 1
  fi

  unshare -U -r "$@"
  exit 0
}

function create_compat {
  local FILE="$1"
  echo 11 > "${FILE}"
}

function create_changelog {
  local FILE="$1"
  local PACKAGE_NAME="$2"

  cat > "${FILE}" <<EOF
${PACKAGE_NAME} (0.0-0) UNRELEASED; urgency=medium
EOF
}

function create_synthetic_files {
  local DEBIAN_DIR="$1"
  local PACKAGE_NAME="$(cat "${DEBIAN_DIR}/control" | grep 'Source: ' | sed -E 's/^[^:]+:\s*//')"

  [ -f "${DEBIAN_DIR}/compat" ] || create_compat "${DEBIAN_DIR}/compat"
  [ -f "${DEBIAN_DIR}/changelog" ] || create_changelog "${DEBIAN_DIR}/changelog" "${PACKAGE_NAME}"
}

function build {
  local BUILD_DIR="$1"

  local COMPONENT=$(jq -r '.component' "${BUILD_DIR}/.buildme")
  COMPONENT=${COMPONENT:-main}

  local TMP_DIR=$(mktemp -d)
  trap "rm -rf ${TMP_DIR}" RETURN INT TERM EXIT

  mkdir -p "${TMP_DIR}/.build/debian"
  rsync -a --exclude '.*' "${BUILD_DIR}/" "${TMP_DIR}/.build/debian"

  create_synthetic_files "${TMP_DIR}/.build/debian"

  (cd "${TMP_DIR}/.build/" && dpkg-buildpackage -uc -us -b)

  mkdir -p "${ARTIFACTS_DIR}/${COMPONENT}"
  cp "${TMP_DIR}/"*.deb "${ARTIFACTS_DIR}/${COMPONENT}/"
}

function build_required {
  local package_dir="$1"

  local NAME=$(jq -r '.package' "${package_dir}/.buildme")
  local VERSION=$(jq -r '.version' "${package_dir}/.buildme")

  local COMPONENT=$(jq -r '.component' "${package_dir}/.buildme")
  COMPONENT=${COMPONENT:-main}

  if [[ ! -f "${ARTIFACTS_DIR}/${COMPONENT}/${NAME}_${VERSION}_amd64.deb" ]]; then
    return 0
  else
    return 1
  fi
}


check_prerequisites
unshare_me "$0" "$@"

mkdir -p "${ARTIFACTS_DIR}"

SEARCH_DIR="."

if [ "$#" -gt 1 ]; then
  echo "usage: $0 [search_dir]"
  exit 1
elif [ "$#" -eq 1 ]; then
  SEARCH_DIR="$1"
fi

find "${SEARCH_DIR}" -name .buildme | while read -r dir; do
  package_dir="$(realpath "$(dirname "${dir}")")"

  APP_NAME=$(jq -r '.package' "${package_dir}/.buildme")
  if build_required "${package_dir}"; then
    echo "Build required for: ${APP_NAME}"
    if [ ! -f "${package_dir}/helper" ]; then
      : # no pre-checks existing
    elif (cd "${package_dir}" && ./helper check >/dev/null 2>&1); then
      echo "  build pre-checks succeeded"
    else
      echo "  build pre-checks failed"
      exit 1
    fi
  else
    echo "No build required for: ${APP_NAME}"
  fi
done

find "${SEARCH_DIR}" -name .buildme | while read -r dir; do
  package_dir="$(realpath "$(dirname "${dir}")")"

  if build_required "${package_dir}"; then
    build "${package_dir}"
  fi
done
