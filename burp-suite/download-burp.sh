#!/bin/bash

get_version() {
  url="https://portswigger.net/burp/releases/community/latest"
  curl -w '%{url_effective}' -L -s -S "${url}" -o /dev/null \
      | perl -n -e'/-(\d+(-\d+)+)\?/ && print $1' \
      | sed 's/-/./g'
}

download() {
  url="https://portswigger.net/burp/releases/download?product=community&version=$1&type=Jar"
  curl ${CURL_OPTIONS} -o "$2" "${url}"
}

fail() {
  echo $1 >&2; exit 1
}

[ "$#" -eq 2 ] || fail "usage: $0 <action> <dest-file>"
ACTION="$1"
DEST_FILE="$2"

VERSION=$(get_version)

case ${ACTION} in
  version)
    echo ${VERSION}
    ;;
  download)
    download "${VERSION}" "${DEST_FILE}"
    ;;
  check)
    # 'content_type' would be another possibility: should be application/octet-stream
    STATUS=$(CURL_OPTIONS="-I -s -w %{response_code}" download "${VERSION}" "${DEST_FILE}")
    [ "${STATUS}" == "200" ] || fail "download failed"
    ;;
esac